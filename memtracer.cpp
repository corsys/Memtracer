
/*! @file
 *  This is an example of the PIN tool that demonstrates some basic PIN APIs 
 *  and could serve as the starting point for developing your first PIN tool
 */

#include "pin.H"
#include <iostream>
#include <fstream>
#include <dlfcn.h>
#include "instlib.H"
#include "memtracer.H"

LOCALVAR MEMTRACER memtracer;

using namespace INSTLIB; 

LOCALVAR ofstream *outfile;
LOCALVAR ofstream *malloc_out;
LOCALVAR ofstream *page_out;
LOCALVAR ofstream *ap_info_out;
LOCALVAR ofstream *ap_map_out;
LOCALVAR ofstream *mem_pipe_out;
LOCALVAR ofstream *mem_info_out;
LOCALVAR ifstream *phase_desc_in;
LOCALVAR ifstream *phase_weight_in;
LOCALVAR ifstream *phase_labels_in;
LOCALVAR ifstream *short_weight_in;

/* ================================================================== */
// Global variables 
/* ================================================================== */

std::ostream * out = &cerr;

/* ===================================================================== */
// Command line switches
/* ===================================================================== */

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE,    "pintool",
    "o", "dcache.out", "specify dcache file name");
KNOB<BOOL> KnobVirtualAddresses(KNOB_MODE_WRITEONCE, "pintool",
    "V","0", "trace virtual addresses instead of physical addresses");
KNOB<BOOL> KnobSmallCache(KNOB_MODE_WRITEONCE, "pintool",
    "S","0", "use a smaller L1 cache");
KNOB<BOOL> KnobDisableCache(KNOB_MODE_WRITEONCE, "pintool",
    "N","0", "disable caching");
KNOB<BOOL> KnobDumpTrace(KNOB_MODE_WRITEONCE, "pintool",
    "D","0", "dump the cputrace to a file (or to the mem pipe)");
KNOB<BOOL> KnobDumpTiers(KNOB_MODE_WRITEONCE, "pintool",
    "R","0", "dump tier information in the cputrace");
KNOB<BOOL> KnobDumpMunmap(KNOB_MODE_WRITEONCE, "pintool",
    "M","0", "dump munmap information in the cputrace");
KNOB<BOOL> KnobAllTier1(KNOB_MODE_WRITEONCE, "pintool",
    "A","0", "annotate everything as tier 1 in the cputrace");
KNOB<BOOL> KnobAllTier2(KNOB_MODE_WRITEONCE, "pintool",
    "B","0", "annotate everything as tier 2 in the cputrace");
KNOB<BOOL> KnobSliceTrace(KNOB_MODE_WRITEONCE, "pintool",
    "T","0", "only trace memory accesses from phases");
KNOB<BOOL> KnobDumpMallocs(KNOB_MODE_WRITEONCE, "pintool",
    "Y","0", "collect the malloc trace file");
KNOB<BOOL> KnobDumpPages(KNOB_MODE_WRITEONCE, "pintool",
    "Z","0", "collect the page trace file");

KNOB<BOOL> KnobDumpAPRW(KNOB_MODE_WRITEONCE, "pintool",
    "Q","0", "differentiate reads and writes when dumping apinfo");

KNOB<UINT32> KnobTier1Limit(KNOB_MODE_WRITEONCE, "pintool",
    "L","0", "page limit for tier1 memory (0 is unlimited)");
KNOB<UINT32> KnobForceMode(KNOB_MODE_WRITEONCE, "pintool",
    "F","0", "force memory onto tier 1 or tier 2 (0 is unforced)");

KNOB<UINT64> KnobPhaseLength(KNOB_MODE_WRITEONCE, "pintool",
    "P","100000000", "set the length of phases (defaults to 100M)");
KNOB<UINT64> KnobShortPhaseLength(KNOB_MODE_WRITEONCE, "pintool",
    "H","100000000", "set the length of phases (defaults to 100M)");

KNOB<string>KnobStatFileName(KNOB_MODE_WRITEONCE,  "pintool",
                     "statfile", "memtracer.out", "Name of the memtracer stats file.");

KNOB<string>KnobPhaseDescFileName(KNOB_MODE_WRITEONCE,  "pintool",
                     "phase_desc_file", "",
                     "Name of the phase description file.");

KNOB<string>KnobPhaseWeightFileName(KNOB_MODE_WRITEONCE,  "pintool",
                     "phase_weight_file", "",
                     "Name of the phase weight file.");

KNOB<string>KnobPhaseLabelsFileName(KNOB_MODE_WRITEONCE,  "pintool",
                     "phase_labels_file", "",
                     "Name of the short phase description file.");

KNOB<string>KnobShortWeightFileName(KNOB_MODE_WRITEONCE,  "pintool",
                     "short_weight_file", "",
                     "Name of the short phase weight file.");

KNOB<string>KnobMallocOutFileName(KNOB_MODE_WRITEONCE,  "pintool",
                     "malloc_info_file", "malloc_trace.out",
                     "Name of the memtracer malloc file.");

KNOB<string>KnobMemPipe(KNOB_MODE_WRITEONCE,  "pintool",
                     "mem_pipe", "",
                     "Name of the memory trace pipe.");

KNOB<string>KnobMemInfoFileName(KNOB_MODE_WRITEONCE,  "pintool",
                     "mem_info_file", "meminfo.txt",
                     "Name of the meminfo file.");

KNOB<string>KnobPageOutFileName(KNOB_MODE_WRITEONCE,  "pintool",
                     "page_info_file", "page_trace.out",
                     "Name of the memtracer page file.");

KNOB<string>KnobAPInfoFileName(KNOB_MODE_WRITEONCE,  "pintool",
                     "ap_info", "run_ap_info.out",
                     "Name of the alloc point info file.");

KNOB<string>KnobAPMapFileName(KNOB_MODE_WRITEONCE,  "pintool",
                     "ap_map", "run_ap_map.out",
                     "Name of the alloc point map file.");

/* ===================================================================== */
// Utilities
/* ===================================================================== */

INT32 Usage()
{
    cerr <<
        "This pin tool is a simple PinPlay-enabled memory tracer\n"
        "\n";

    cerr << KNOB_BASE::StringKnobSummary() << endl;
    return -1;
}

static void threadCreated(THREADID threadIndex, CONTEXT *, INT32 , VOID *)
{
    if (threadIndex > 0)
    {
        cerr << "More than one thread detected. This tool currently works only for single-threaded programs/pinballs." << endl;
        exit(0);
    }
}

int main(int argc, char *argv[])
{
    PIN_InitSymbols();
    if( PIN_Init(argc,argv) )
    {
        return Usage();
    }

    outfile = new ofstream(KnobStatFileName.Value().c_str());

    malloc_out = NULL;
    if (KnobDumpMallocs) {
        malloc_out = new ofstream(KnobMallocOutFileName.Value().c_str());
    }

    page_out = NULL;
    if (KnobDumpPages) {
        page_out = new ofstream(KnobPageOutFileName.Value().c_str());
    }

    ap_info_out = new ofstream(KnobAPInfoFileName.Value().c_str());
    
    ap_map_out = new ofstream(KnobAPMapFileName.Value().c_str());
    mem_info_out = new ofstream(KnobMemInfoFileName.Value().c_str());

    mem_pipe_out = NULL;
    if (strcmp(KnobMemPipe.Value().c_str(), "") != 0) {
        mem_pipe_out = new ofstream(KnobMemPipe.Value().c_str());
    }

    phase_desc_in = NULL;
    if (strcmp(KnobPhaseDescFileName.Value().c_str(), "") != 0) {
        phase_desc_in = new ifstream(KnobPhaseDescFileName.Value().c_str());
        if (phase_desc_in->fail()) {
            cout << "error opening phase description file: "
                 <<  KnobPhaseDescFileName.Value().c_str() << endl;
            return -1;
        }
    }

    phase_weight_in = NULL;
    if (strcmp(KnobPhaseWeightFileName.Value().c_str(), "") != 0) {
        phase_weight_in = new ifstream(KnobPhaseWeightFileName.Value().c_str());
        if (phase_weight_in->fail()) {
            cout << "error opening phase weight file: "
                 <<  KnobPhaseWeightFileName.Value().c_str() << endl;
            return -1;
        }
    }

    phase_labels_in = NULL;
    if (strcmp(KnobPhaseLabelsFileName.Value().c_str(), "") != 0) {
        phase_labels_in = new ifstream(KnobPhaseLabelsFileName.Value().c_str());
        if (phase_labels_in->fail()) {
            cout << "error opening phase description file: "
                 <<  KnobPhaseLabelsFileName.Value().c_str() << endl;
            return -1;
        }
    }

    short_weight_in = NULL;
    if (strcmp(KnobShortWeightFileName.Value().c_str(), "") != 0) {
        short_weight_in = new ifstream(KnobShortWeightFileName.Value().c_str());
        if (short_weight_in->fail()) {
            cout << "error opening phase weight file: "
                 <<  KnobShortWeightFileName.Value().c_str() << endl;
            return -1;
        }
    }

    memtracer.Activate(KnobDumpAPRW, KnobVirtualAddresses, KnobSmallCache, KnobDisableCache, 
                       KnobDumpTrace, KnobDumpTiers, KnobDumpMunmap, 
                       KnobDumpMallocs, KnobDumpPages, KnobSliceTrace, 
                       KnobTier1Limit.Value(), KnobPhaseLength.Value(), 
                       KnobShortPhaseLength.Value(), KnobForceMode.Value(),
                       outfile, malloc_out, page_out, ap_info_out, ap_map_out,
                       mem_info_out, mem_pipe_out, phase_desc_in,
                       phase_weight_in, phase_labels_in, short_weight_in);
 
    PIN_AddThreadStartFunction(threadCreated, reinterpret_cast<void *>(0));

    PIN_StartProgram();
}

/* ===================================================================== */
/* eof */
/* ===================================================================== */
