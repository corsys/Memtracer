/*BEGIN_LEGAL 
BSD License 

Copyright (c)2013 Intel Corporation. All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.  Redistributions
in binary form must reproduce the above copyright notice, this list of
conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.  Neither the name of
the Intel Corporation nor the names of its contributors may be used to
endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE INTEL OR
ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
END_LEGAL */

//
// @ORIGINAL_AUTHORS: Harish Patil 
//

#include <iostream>
#include <fstream>

#include "pin.H"
#include "instlib.H"
#include "memtracer.H"
#include "pinplay.H"

LOCALVAR MEMTRACER memtracer;

using namespace INSTLIB; 

LOCALVAR ofstream *outfile;

#define KNOB_LOG_NAME  "log"
#define KNOB_REPLAY_NAME "replay"
#define KNOB_FAMILY "pintool:pinplay-driver"


PINPLAY_ENGINE pinplay_engine;

KNOB_COMMENT pinplay_driver_knob_family(KNOB_FAMILY, "PinPlay Driver Knobs");

KNOB<BOOL>KnobReplayer(KNOB_MODE_WRITEONCE, KNOB_FAMILY,
                       KNOB_REPLAY_NAME, "0", "Replay a pinball");
KNOB<BOOL>KnobLogger(KNOB_MODE_WRITEONCE,  KNOB_FAMILY,
                     KNOB_LOG_NAME, "0", "Create a pinball");

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE,    "pintool",
    "o", "dcache.out", "specify dcache file name");
KNOB<UINT32> KnobCacheSize(KNOB_MODE_WRITEONCE, "pintool",
    "c","512", "cache size in kilobytes");
KNOB<UINT32> KnobLineSize(KNOB_MODE_WRITEONCE, "pintool",
    "b","64", "cache block size in bytes");
KNOB<UINT32> KnobAssociativity(KNOB_MODE_WRITEONCE, "pintool",
    "a","8", "cache associativity (1 for direct mapped)");
KNOB<BOOL> KnobTraceAllMemOps(KNOB_MODE_WRITEONCE, "pintool",
    "M","0", "trace all mem ops -- not just cache misses");
KNOB<BOOL> KnobVirtualAddresses(KNOB_MODE_WRITEONCE, "pintool",
    "V","0", "trace virtual addresses instead of physical addresses");

KNOB<UINT64> KnobPhases(KNOB_MODE_WRITEONCE, 
                        "pintool",
                        "phaselen",
                        "100000000", 
                        "Print memtrace stats every these many instructions (and also at the end).\n");
KNOB<string>KnobStatFileName(KNOB_MODE_WRITEONCE,  "pintool",
                     "statfile", "memtracer.out", "Name of the memtracer stats file.");
KNOB<BOOL> KnobCpuMode(KNOB_MODE_WRITEONCE, "pintool",
                    "P", "0", "generate output in Ramulator cpu format.");


INT32 Usage()
{
    cerr <<
        "This pin tool is a simple PinPlay-enabled memory tracer\n"
        "\n";

    cerr << KNOB_BASE::StringKnobSummary() << endl;
    return -1;
}

static void threadCreated(THREADID threadIndex, CONTEXT *, INT32 , VOID *)
{
    if (threadIndex > 0)
    {
        cerr << "More than one thread detected. This tool currently works only for single-threaded programs/pinballs." << endl;
        exit(0);
    }
}


int main(int argc, char *argv[])
{
    PIN_InitSymbols();
    if( PIN_Init(argc,argv) )
    {
        return Usage();
    }

    outfile = new ofstream(KnobStatFileName.Value().c_str());
    memtracer.Activate(KnobPhases, KnobCacheSize, KnobLineSize,
                       KnobAssociativity, KnobTraceAllMemOps, KnobVirtualAddresses,
                       outfile, KnobCpuMode); 

    pinplay_engine.Activate(argc, argv, KnobLogger, KnobReplayer);
    if(KnobLogger)
    {
        cout << "Logger basename " << pinplay_engine.LoggerGetBaseName() 
            << endl;
    }
    if(KnobReplayer)
    {
        cout << "Replayer basename " << pinplay_engine.ReplayerGetBaseName() 
            << endl;
    }

    PIN_AddThreadStartFunction(threadCreated, reinterpret_cast<void *>(0));


    PIN_StartProgram();
}
